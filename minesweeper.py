def main():
    from os import environ
    environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"
    import pygame
    pygame.init()
    from time import time
    from random import randint

    fonts = {30: pygame.font.SysFont('Arial', 30), 40: pygame.font.SysFont('Arial', 40)}

    def text(text_x, text_y, string, color, size): # draw text
        window.blit(fonts[size].render(string, True, color), (text_x, text_y))

    def resize_window(window_width, window_height): # resize window
        window = pygame.display.set_mode((window_width, window_height))
        return (window_width, window_height)

    def update_rects(): # update positions of rects
        beginner_rect = pygame.Rect(0, 0, 135, 65)
        intermediate_rect = pygame.Rect((window_width / 2) - 80, 0, 175, 65)
        expert_rect = pygame.Rect(window_width - 105, 0, 200, 65)
        start_rect = pygame.Rect((window_width / 2) - 30, 88, 50, 50)
        game_rect = pygame.Rect(0, 160, window_width, window_height - 160)
        return (start_rect, beginner_rect, intermediate_rect, expert_rect, game_rect)

    def choose_difficulty(name, text_x): # draw difficulty text
        if difficulty == name:
            color = "white"
        else:
            color = "black"
        text(text_x, 18, name.capitalize(), color, 30)

    def draw_squares(): # draw squares
        number_colors = {
            "1": "blue",
            "2": "green4",
            "3": "red",
            "4": "darkslateblue",
            "5": "darkred",
            "6": "darkslategray4",
            "7": "black",
            "8": "grey35"
        }

        for i, row in enumerate(grid):
            for j, square in enumerate(row):
                x = (j * square_size)
                y = (i * square_size)

                # choose square background
                if square == "M" and game_over and (i, j) == (clicked_square):
                    square_color = "red"
                elif "H" in square and "F" not in square:
                    square_color = "grey75"
                elif "?" in square or "F" in square or "M" in square:
                    square_color = "grey80"
                else:
                    square_color = "grey65"

                # draw square background
                pygame.draw.rect(window, square_color, (x, 160 + y, square_size, square_size))
                    
                if square == "M": # draw mine
                    mine_center = ((x + square_size / 2), (160 + y + square_size / 2))
                    pygame.draw.circle(window, "black", (mine_center[0], mine_center[1]), square_size / 3.25)
                    pygame.draw.line(window, "black", (mine_center[0], mine_center[1] - square_size / 2), (mine_center[0], mine_center[1] + square_size / 2), 5)
                    pygame.draw.line(window, "black", (mine_center[0] - square_size / 2, mine_center[1]), (mine_center[0] + square_size / 2, mine_center[1]), 5)
                    pygame.draw.line(window, "black", (mine_center[0] - square_size / 3, mine_center[1] - square_size / 3), (mine_center[0] + square_size / 3, mine_center[1] + square_size / 3), 6)
                    pygame.draw.line(window, "black", (mine_center[0] + square_size / 3, mine_center[1] - square_size / 3), (mine_center[0] - square_size / 3, mine_center[1] + square_size / 3), 6)

                elif "F" in square: # draw flag
                    line_start = (x + square_size / 2, 160 + y + square_size / 2)
                    line_end = (x + square_size / 2, 160 + y + square_size)
                    pygame.draw.line(window, "black", line_start, line_end, 3)

                    point1 = ((x + 1 + square_size / 2, (160 + y) + 5))
                    point2 = ((x + 1 + square_size / 6, (160 + y) + square_size / 3.25))
                    point3 = ((x + 1 + square_size / 2, (160 + y) + square_size / 2))
                    pygame.draw.polygon(window, "red", (point1, point2, point3))

                    if game_over and "M" not in square: # draw cross over flag
                        line_start = (x, 160 + y)
                        line_end = (x + square_size, 160 + y + square_size)
                        pygame.draw.line(window, "red", line_start, line_end, 4)

                        line_start = (x + square_size, 160 + y)
                        line_end = (x, 160 + y + square_size)
                        pygame.draw.line(window, "red", line_start, line_end, 4)

                elif "?" not in square and any(number in square for number in "12345678"): # draw number
                    text((x + number_font - number_font / 2.5), (160 + y + number_font - number_font / 1.5), square, number_colors[square], number_font)

    def draw_grid(): # draw a grid to seperate squares
        line_width = 3
        line_color = "gray50"

        for i in range(grid_height):
            line_start = (0, 160 + square_size + (i * square_size))
            line_end = (window_width, 160 + square_size + (i * square_size))
            pygame.draw.line(window, line_color, line_start, line_end, line_width)
        
        for i in range(grid_width):
            line_start = (0 + square_size + (i * square_size), 160)
            line_end = ((0 + square_size + (i * square_size)), window_height)
            pygame.draw.line(window, line_color, line_start, line_end, line_width)

    def draw_start_button(): # draw smiley
        x = (window_width / 2)
        pygame.draw.rect(window, "black", (x - 30, 88, 50, 50))
        pygame.draw.rect(window, "grey40", (x - 26, 92, 42, 42))
        pygame.draw.circle(window, "yellow", (x - 5, 113), 20)
        if not won:
            pygame.draw.circle(window, "black", (x - 13, 105), 4)
            pygame.draw.circle(window, "black", (x + 3, 105), 4)
        if not game_over:
            if not won:
                pygame.draw.circle(window, "black", (x - 13, 118), 3)
                pygame.draw.circle(window, "black", (x + 3, 118), 3)
            pygame.draw.circle(window, "black", (x - 10, 122), 3)
            pygame.draw.circle(window, "black", (x + 0, 122), 3)
            pygame.draw.circle(window, "black", (x - 3, 124), 3)
            pygame.draw.circle(window, "black", (x - 7, 124), 3)
            pygame.draw.circle(window, "black", (x - 5, 125), 2)
        if won:
            pygame.draw.circle(window, "black", (x - 23, 110), 2)
            pygame.draw.circle(window, "black", (x + 13, 110), 2)
            pygame.draw.circle(window, "black", (x - 21, 107), 2)
            pygame.draw.circle(window, "black", (x + 11, 107), 2)
            a = 0
            for i in range(15):
                pygame.draw.circle(window, "black", (x - (19 - a), 104), 2)
                pygame.draw.circle(window, "black", (x + (9 - a), 104), 2)
                a += 2
            pygame.draw.circle(window, "black", (x - 11, 107), 5)
            pygame.draw.circle(window, "black", (x + 2, 107), 5)
        if game_over:
            pygame.draw.circle(window, "black", (x - 13, 124), 3)
            pygame.draw.circle(window, "black", (x + 2, 124), 3)
            pygame.draw.circle(window, "black", (x - 10, 122), 3)
            pygame.draw.circle(window, "black", (x - 1, 122), 3)
            pygame.draw.circle(window, "black", (x - 3, 121), 3)
            pygame.draw.circle(window, "black", (x - 7, 121), 3)
            pygame.draw.circle(window, "black", (x - 5, 121), 3)

    def draw_border(): # draw a border around the window
        thickness = 4
        color = "grey40"
        pygame.draw.rect(window, color, (0, 0, window_width, thickness))
        pygame.draw.rect(window, color, (window_width - thickness, 0, thickness, window_height))
        pygame.draw.rect(window, color, (0, window_height - thickness, window_width, thickness))
        pygame.draw.rect(window, color, (0, 0, thickness, window_height))

    def clicked_rect(rectangle): # check if clicked on a rect
        if rectangle.collidepoint(mouse_pos) and left_click:
            return "left"
        elif rectangle.collidepoint(mouse_pos) and middle_click:
            return "middle"
        elif rectangle.collidepoint(mouse_pos) and right_click:
            return "right"

    # setup
    max_fps = 45
    prev_difficulty = "beginner"
    difficulty = "beginner"
    square_size = 60
    grid_width = 9
    grid_height = 9
    mines = 10

    window_width, window_height = (grid_width * square_size, 160 + grid_height * square_size)
    window = pygame.display.set_mode((window_width, window_height))
    pygame.display.set_caption("Minesweeper")

    timer = 0
    grid = [["?" for i in range(grid_width)] for j in range(grid_height)]
    start_rect, beginner_rect, intermediate_rect, expert_rect, game_rect = update_rects()
    number_font = int(square_size / 1.65)
    fonts[number_font] = pygame.font.SysFont('Arial', number_font)
    running = True
    playing = False
    game_over = False
    won = False
    clock = pygame.time.Clock()

    # game loop
    while running:
        # limit fps
        clock.tick(max_fps)

        # get mouse position
        mouse_pos = pygame.mouse.get_pos()

        # check for clicks
        left_click = False
        middle_click = False
        right_click = False

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 3:
                    right_click = True
            elif event.type == pygame.MOUSEBUTTONUP:
                if event.button == 1:
                    left_click = True
                elif event.button == 2:
                    middle_click = True

        holding_left_mouse, holding_middle_mouse, holding_right_mouse = pygame.mouse.get_pressed()[:3]

        # draw background
        window.fill("grey65")

        # draw difficulty text
        pygame.draw.rect(window, "grey40", (0, 65, window_width, 15))
        choose_difficulty("beginner", 10)
        choose_difficulty("intermediate", (window_width / 2) - 75)
        choose_difficulty("expert", window_width - 100)

        # draw mines remaining, timer, smiley
        pygame.draw.rect(window, "grey40", (0, 145, window_width, 15))
        text(15, 90, str(mines).zfill(3), "red", 40)
        text(window_width - 85, 90, str(int(timer / 1)).zfill(3), "red", 40)
        draw_start_button()

        for i, row in enumerate(grid): # remove 'holding' from squares
            for j, square in enumerate(row):
                if "H" in square:
                    grid[i][j] = square.replace("H", "")

        if any((holding_left_mouse, holding_right_mouse, holding_middle_mouse)) and not game_over: # set clicked square if clicked on grid
            clicked_square = (((mouse_pos[1] - 160) // square_size), mouse_pos[0] // square_size)

        if holding_left_mouse and mouse_pos[1] > 160 and not game_over and not won: # add 'holding' to square if holding left mouse 
            square = grid[clicked_square[0]][clicked_square[1]]
            if "?" in square and "H" not in square:
                grid[clicked_square[0]][clicked_square[1]] += "H"

        elif clicked_rect(beginner_rect) == "left": # check if clicked on beginner
            difficulty = "beginner"

        elif clicked_rect(intermediate_rect) == "left": # check if clicked on intermediate
            difficulty = "intermediate"

        elif clicked_rect(expert_rect) == "left": # check if clicked on expert
            difficulty = "expert"

        elif clicked_rect(start_rect) == "left": # check if clicked on smiley
            # set values
            if difficulty == "beginner":
                grid_width = 9
                grid_height = 9
                mines = 10
            elif difficulty == "intermediate":
                grid_width = 16
                grid_height = 16
                mines = 40
            else:
                grid_width = 30
                grid_height = 16
                mines = 99
            timer = 0
            
            # make empty grid
            grid = [["?" for i in range(grid_width)] for j in range(grid_height)]

            # resize window and update rect positions
            if prev_difficulty != difficulty:
                window_width, window_height = resize_window(grid_width * square_size, 160 + grid_height * square_size)
                start_rect, beginner_rect, intermediate_rect, expert_rect, game_rect = update_rects()
            prev_difficulty = difficulty

            playing = False
            game_over = False
            won = False

        elif clicked_rect(game_rect) == "left" and playing == False and not game_over and not won: # check if clicked on grid and start
            # set clicked square to 0
            grid[clicked_square[0]][clicked_square[1]] = "0"

            # create invalid mine positions (around starting square)
            invalid_mine_positions = []
            i, j = clicked_square
            for a in range(i - 1, i + 2):
                for b in range(j - 1, j + 2):
                    if a < 0 or b < 0 or a > grid_height - 1 or b > grid_width - 1:
                        continue
                    invalid_mine_positions.append((a, b))

            # create mine position
            def create_position():
                return (randint(0, grid_height - 1), randint(0, grid_width - 1))

            # create mine positions
            mine_positions = []
            for i in range(mines):
                position = create_position()
                while position in mine_positions or position in invalid_mine_positions:
                    position = create_position()
                mine_positions.append(position)

            # add mines to grid
            for position in mine_positions:
                grid[position[0]][position[1]] += "M"

            # add numbers to grid
            for i, row in enumerate(grid):
                for j, square in enumerate(row):
                    mines_around = 0
                    if "M" in square or "0" in square:
                        continue
                    for a in range(i - 1, i + 2):
                        for b in range(j - 1, j + 2):
                            if a < 0 or b < 0 or a > grid_height - 1 or b > grid_width - 1 or (a, b) == (i, j):
                                continue
                            if "M" in grid[a][b]:
                                mines_around += 1
                    grid[i][j] += str(mines_around)

            time_start = time()
            playing = True

        elif clicked_rect(game_rect) == "left" and playing: # reveal square
            square = grid[clicked_square[0]][clicked_square[1]]
            if "F" not in square:
                grid[clicked_square[0]][clicked_square[1]] = square.replace("?", "")
        
        elif clicked_rect(game_rect) == "right" and playing: # flag square
            square = grid[clicked_square[0]][clicked_square[1]]
            if "?" in square and "F" not in square:
                square += "F"
                mines -= 1
            elif "F" in square:
                square = square.replace("F", "?")
                mines += 1
            grid[clicked_square[0]][clicked_square[1]] = square

        elif middle_click or holding_middle_mouse and mouse_pos[1] > 160 and playing: # middle click on number
            square = grid[clicked_square[0]][clicked_square[1]]
            flags_around = 0
            if "?" not in square and any(number in square for number in "12345678"):
                i, j = clicked_square
                for a in range(i - 1, i + 2):
                    for b in range(j - 1, j + 2):
                        if a < 0 or b < 0 or a > grid_height - 1 or b > grid_width - 1 or (a, b) == (i, j):
                            continue
                        if middle_click:
                            if "F" in grid[a][b]:
                                flags_around += 1

                        if not middle_click:
                            if "?" in grid[a][b] and "H" not in grid[a][b] and "F" not in grid[a][b]:
                                grid[a][b] += "H"

                if flags_around == int(square):
                    for a in range(i - 1, i + 2):
                        for b in range(j - 1, j + 2):
                            if a < 0 or b < 0 or a > grid_height - 1 or b > grid_width - 1 or (a, b) == (i, j):
                                continue
                            if "?" in grid[a][b] and "F" not in grid[a][b]:
                                grid[a][b] = grid[a][b].replace("?", "")
                                if "M" in grid[a][b]:
                                    clicked_square = (a, b)

        if playing:
            for i, row in enumerate(grid): # reveal 0s
                for j, square in enumerate(row):
                    if square == "0":
                        for a in range(i - 1, i + 2):
                            for b in range(j - 1, j + 2):
                                if a < 0 or b < 0 or a > grid_height - 1 or b > grid_width - 1 or (a, b) == (i, j):
                                    continue
                                grid[a][b] = grid[a][b].replace("?", "")

            for i, row in enumerate(grid): # check for loss
                for j, square in enumerate(row):
                    if not game_over and "M" in grid[i][j] and "?" not in grid[i][j]:
                        game_over = True
                        playing = False
                        for k, row in enumerate(grid):
                            for l, square in enumerate(row):
                                if "M" in square:
                                    grid[k][l] = grid[k][l].replace("?", "")

            found_not_discovered_square = False # check for win
            for i, row in enumerate(grid): 
                for j, square in enumerate(row):
                    if any(number in square for number in "012345678"):
                        if "?" in square:
                            found_not_discovered_square = True
            
            if not found_not_discovered_square:
                playing = False
                won = True
                for i, row in enumerate(grid): # flag all mines
                    for j, square in enumerate(row):
                        if "M" in square and "F" not in square:
                            mines -= 1
                            grid[i][j] += "F"

            # increase time
            if timer < 999:
                timer = time() - time_start
            else:
                timer = 999

        # draw squares, grid, border
        draw_squares()
        draw_grid()
        draw_border()

        # update window
        pygame.display.update()
    pygame.quit()

if __name__ == "__main__":
    main()